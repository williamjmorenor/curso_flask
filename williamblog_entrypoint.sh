#! /bin/sh
# Aseguramos que la versión de la base de datos sea la última.
/usr/local/bin/flask db upgrade

# -b 0.0.0.0:8000 permite a la aplicación servir solicitudes
# fuera del locahost, sin esta opción la aplicación no es accesible
# fuera del contenedor que ejecuta la aplicación.
/usr/local/bin/gunicorn -w 2 -b 0.0.0.0:8000 william_blog:app
