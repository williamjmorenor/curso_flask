import os

"""
Un solo objeto para la configuración de la aplicacion.

Si existe una directiva de configuración de la variable en las variables
del sistema se prefiere usar esta sobre una variable hardcode en el codigo
de la aplicación.
"""

DIRECTORIO_BASE= os.path.abspath(os.path.dirname(__file__))
DB_DESARROLLO = 'sqlite:///' + os.path.join(DIRECTORIO_BASE, 'app.db')

SECRET_KEY = os.environ.get("FLASK_SECRET_KEY") or "th1s1s4s4cr3tk3y"
SQLALCHEMY_DATABASE_URI = os.environ.get("FLASK_DATABASE_URL") or DB_DESARROLLO
SQLALCHEMY_TRACK_MODIFICATIONS = False