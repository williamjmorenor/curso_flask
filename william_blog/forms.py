"""Aqui se definen las formas de la aplicación."""

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired

class LoginForm(FlaskForm):
	"""Pantalla de inicio de sesión"""

	usuario = StringField("Usuario", validators=[DataRequired()])
	contraseña = PasswordField("Contraseña", validators=[DataRequired()])
	mantener_sesion = BooleanField("Mantener sesion iniciada")
	iniciar_sesion = SubmitField("Iniciar Sesión")
