"""Iniciamos la aplicación que sera servida por el servidor de aplicaciones."""

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from william_blog import config

app = Flask(__name__)
app.config.from_object(config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# Importamos aquí para evitar un error de importación circular.
from william_blog import routes, models
