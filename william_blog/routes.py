
"""Esta es la parte visible al usario de la aplocacion."""

from flask import flash, redirect, render_template

from william_blog import app
from william_blog.forms import LoginForm


@app.route("/")
@app.route("/home")
@app.route("/index")
def index():
    """La página de inicio de la aplicación."""
    return render_template(
        "index.html",
        site="William Blog",
        name="William Moreno Reyes."
        )

@app.route('/manifest')
def manifest():
    """Manifiesto de archivos para guardar en cache."""
    manifiesto = render_template("william_blog.appcache")
    manifiesto.headers["Content-Type"] = "text/cache-manifest"
    return manifiesto

@app.route("/login", methods=["GET", "POST"])
def login():
    """Muestra la pagina de inicio."""
    formulario = LoginForm()
    if formulario.validate_on_submit():
        flash("Loging required for user {}", format(formulario.usuario.data))
        return redirect("/index")
    return render_template("login.html",
        form=formulario,
        site = "Inicio de Sesión.",
        html_class = "login-pf"
        )
