from william_blog import db

class Usuario(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(65), index=True, unique=True)
	email = db.Column(db.String(100), index=True, unique=True)
	password = db.Column(db.String(128))

	def __repr__(self):
		return '<User {}>'.format(self.username)