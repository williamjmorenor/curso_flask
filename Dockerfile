FROM python:3-alpine3.6 

MAINTAINER William Moreno Reyes "williamjmorenor@gmail.com"

COPY migrations /src/migrations
COPY william_blog /src/william_blog
COPY requirements.txt /src/requirements.txt
COPY setup.py /src/setup.py
COPY williamblog_entrypoint.sh /src/williamblog_entrypoint.sh

WORKDIR /src

RUN pip install --no-cache-dir -r requirements.txt \
    && rm -rf dist \
    && rm -rf ~/.cache/pip

RUN python setup.py develop && rm -rf dist

EXPOSE 8000

# -b 0.0.0.0:8000 permite a la aplicación servir solicitudes
# fuera del locahost, sin esta opción la aplicación no es accesible
# fuera del contenedor que ejecuta la aplicación.
CMD /bin/sh /src/williamblog_entrypoint.sh
